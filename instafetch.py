'''
Copyright 2020 Johannes Rehnman <johannes@distraktion.se>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


This program formats Instagram pages as a RSS feed
'''

from dataclasses import dataclass
from datetime import datetime
import argparse
import cgi
import json
import re
import requests


@dataclass
class Post:
    '''
    Python representation of a single instagram post
    '''
    url: str
    caption: str
    alt_text: str
    taken_at_timestamp: datetime
    shortcode: str


def fetch_url(url):
    '''
    Convenience method for fetching an url over http and raise exception on
    any HTTP error.
    '''
    req = requests.get(url)
    req.raise_for_status()
    return req.text


def count_parenthesises(codestr):
    '''
    Searches for where a {}-delimited block ends. Returns number of characters
    in string that block encompasses.
    '''
    code_length = 0
    num_parens = 0
    skip = False
    for char in codestr:
        if skip:
            skip = False
        else:
            if char == '\\':
                skip = True

            if char == '{':
                num_parens += 1
            if char == '}':
                num_parens -= 1

        code_length += 1
        if num_parens <= 0:
            return code_length

    raise Exception()


def find_data(html):
    '''
    Finds javascript variable from on instagram html containing photo stream
    json data. Takes HTML input as argument and returns JSON as string.
    '''
    match = re.search(r'_sharedData\s*=\s*', html)
    if not match:
        raise Exception()
    (_, matchend) = match.span()

    jsonlen = count_parenthesises(html[matchend:])
    jsonstr = html[matchend:matchend+jsonlen]
    return jsonstr


def parse_posts(jsonstr):
    '''
    Parses instagram-internal JSON into Python object
    '''
    obj = json.loads(jsonstr)
    edges = obj['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']
    for edge in edges:
        alt = edge['node'].get('accessibility_caption') or ''
        timestamp = datetime.fromtimestamp(edge['node']['taken_at_timestamp'])
        caption_node = edge['node'].get('edge_media_to_caption')
        if caption_node and caption_node['edges']:
            caption = caption_node['edges'][0]['node']['text'] or ''
        else:
            caption = ''

        yield Post(url=edge['node']['display_url'],
                   caption=caption,
                   alt_text=alt,
                   taken_at_timestamp=timestamp,
                   shortcode=edge['node']['shortcode'])


def format_rss(user, items):
    '''
    Generator that converts a list of Post objects into a RSS-stream.
    Intended to be called as

        s = ''.join(format_rss(username, items))
    '''

    yield f'''
        <rss>
            <channel>
                <title>{cgi.html.escape(user)}</title>
                <link>https://www.instagram.com/{user}/</link>
        '''

    for post in items:
        yield f'''
                    <item>
                        <title>{cgi.html.escape(post.alt_text)}</title>
                        <description>
                            <![CDATA[
                                <img src="{post.url}"
                                     alt="{cgi.html.escape(post.alt_text)}"/>
                                <div><p>{cgi.html.escape(post.caption)}</p></div>
                            ]]>
                        </description>
                        <link>https://www.instagram.com/p/{post.shortcode}/</link>
                        <pubDate>{post.taken_at_timestamp}</pubDate>
                        <guid>https://www.instagram.com/p/{post.shortcode}/</guid>
                    </item>
                '''

    yield r'''
            </channel>
        </rss>
        '''


def instafetch(user):
    '''
    API entry point, fetches from instagram user and returns unicode
    string containing corresponding RSS-feed
    '''
    url = f'https://www.instagram.com/{user}'
    html = fetch_url(url)
    jsonstr = find_data(html)
    post_iter = parse_posts(jsonstr)
    rss = ''.join(format_rss(user, post_iter))
    return rss


def main():
    '''
    Command-line entry point. Takes an instagram username as a single argument
    and prints corresponding RSS feed to standard out
    '''
    argparser = argparse.ArgumentParser(description='Get RSS feed for Instagram user')
    argparser.add_argument('username')
    args = argparser.parse_args()

    print(instafetch(args.username))


if __name__ == '__main__':
    main()
